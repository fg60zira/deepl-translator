import deepl
import file_utils
import csv
import deepl 
import json
import pandas as pd


class WikiQA:

    def __init__(self, filename, with_answers, isTesting):
        with open("deepl/new_auth.json") as f:
            data = json.load(f)

        self.translator = deepl.translator.Translator(
            auth_key=data.get("auth_key"), 
            source_lang=deepl.translator.Language.EN, 
            target_lang=deepl.translator.Language.DE)


        if isTesting:
            self.input_file_path = "data/test_input/WikiQACorpus/" + filename
            self.output_file_path = "data/test_output/WikiQACorpus/" + filename
        else:
            self.input_file_path = "data/input_data/WikiQACorpus/" + filename
            self.output_file_path = "data/final_result/WikiQACorpus/" + filename

        self.intermediate_path_EN = "data/intermediate_data/EN_" + filename
        self.intermediate_path_DE = "data/intermediate_data/DE_" + filename
        self.with_answers = with_answers



    def runTranslation(self):
        try: 
            f = open(self.output_file_path)
        except FileNotFoundError as e:
            try:
                f = open(self.intermediate_path_DE)
                size = self.get_input_size()
                self.translate(self.intermediate_path_EN, self.intermediate_path_DE, size)
            except FileNotFoundError as e:
                file_utils.copy_file(self.input_file_path, self.intermediate_path_EN)
                self.init_intermediate()
                size = self.get_input_size()
                self.translate(self.intermediate_path_EN, self.intermediate_path_DE, size)
            


    def init_intermediate(self):
        input_tsv = pd.read_csv(self.intermediate_path_EN, sep = '\t')
        dropped = input_tsv.drop(input_tsv.index, inplace=False)
        dropped.to_csv(self.intermediate_path_DE, sep =  '\t', index=False )


    def get_input_size(self):
        df = pd.read_csv(self.intermediate_path_EN, sep = '\t' )
        size = df.index.stop
        return size 



    def translate(self, input_file, output_file, size):

        cnt = 0
       
        for i in range(size):
            input_tsv = pd.read_csv(input_file, sep = '\t')
            tmp_size = input_tsv.index.stop
            tmp = input_tsv.drop(input_tsv.index[1:tmp_size], inplace=False)
            tmp.reset_index()

            tmp.at[0, 'Question'] = self.translator.translate_text(tmp.at[0, 'Question'])
            tmp.at[0, 'DocumentTitle'] = self.translator.translate_text(tmp.at[0, 'DocumentTitle'])
            if cnt >= 3:
                raise ConnectionError("CONN ERROR")
            tmp.at[0, 'Sentence'] = self.translator.translate_text(tmp.at[0, 'Sentence'])

            if 'AnswerPhrase1' in tmp:
                if not pd.isnull(tmp.at[0, 'AnswerPhrase1']):
                    tmp.at[0, 'AnswerPhrase1'] = self.translator.translate_text(tmp.at[0, 'AnswerPhrase1'])
                if not pd.isnull(tmp.at[0, 'AnswerPhrase2']):
                    tmp.at[0, 'AnswerPhrase2'] = self.translator.translate_text(tmp.at[0, 'AnswerPhrase2'])
                if not pd.isnull(tmp.at[0, 'AnswerPhrase3']):
                    tmp.at[0, 'AnswerPhrase3'] = self.translator.translate_text(tmp.at[0, 'AnswerPhrase3'])
            

            DE = pd.read_csv(output_file, sep =  '\t' )
            newDE = pd.concat([DE, tmp])

            try:
                newDE.to_csv(output_file, sep =  '\t', index=False)
                newEN = input_tsv.drop([0], axis = 0)
                newEN.to_csv(input_file,  sep =  '\t', index=False)
            except Exception as e:
                print("Saving unsuccessfull")
            
            cnt += 1 
        
        self.close_translation()
        self.drop_duplicates()



    def close_translation(self):
        file_utils.copy_file(self.intermediate_path_DE, self.output_file_path)
        file_utils.remove_allFiles("data/intermediate_data")

    def drop_duplicates(self):
        df = pd.read_csv(self.output_file_path, sep='\t')
        df.drop_duplicates()
        df.to_csv(self.output_file_path, sep='\t', index=False)





def dev_test():
    with_answers = False
    filename = "WikiQA-dev.tsv"
    isTesting = True
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()

def test_test():
    with_answers = False
    filename = "WikiQA-test.tsv"
    isTesting = True
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()

def train_test():
    with_answers = False
    filename = "WikiQA-train.tsv"
    isTesting = True
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()

def just_test():
    with_answers = False
    filename = "WikiQA.tsv"
    isTesting = True
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()


def pos_ans_test():
    with_answers = True
    filename = "WikiQASent.pos.ans.tsv"
    isTesting = True
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()

def dev():
    with_answers = False
    filename = "WikiQA-dev.tsv"
    isTesting = False
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()

def test():
    with_answers = False
    filename = "WikiQA-test.tsv"
    isTesting = False
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()

def train():
    with_answers = False
    filename = "WikiQA-train.tsv"
    isTesting = False
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()

def just():
    with_answers = False
    filename = "WikiQA.tsv"
    isTesting = False
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()


def pos_ans():
    with_answers = True
    filename = "WikiQASent.pos.ans.tsv"
    isTesting = False
    t = WikiQA(filename, with_answers, isTesting)
    t.runTranslation()
