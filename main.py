import json
import CoQA
import QuAC
import SQuAD
import WikiQA
import CoQA


#####################################
# WikiQA
#####################################
# File - Size
# dev: 577,1 kB
# test: 1,3 MB 
# train: 4,4 MB
# just: 6,3 MB
# pos_ans: 541,2 kB 
#####################################
# WikiQA.dev()
# WikiQA.test()
# WikiQA.train()
# WikiQA.just()
# WikiQA.pos_ans()


#####################################
# COQA
#####################################
# File-Size
# dev: 9,1 MB
# train: 49,0 MB
#####################################
# CoQA.run_train()
# CoQA.run_dev()


#####################################
# SQuAD
#####################################
# File - Size
# train: 65,6 MB 
# dev: 17,5 MB
#####################################
# SQuAD.run_train()
# SQuAD.run_dev()


#####################################
# QuAC
#####################################
# File - Size
# train: 89,7 MB
# val: 12,5 MB
#####################################
# QuAC.run_train()
# QuAC.run_val()






def run_COQA(testing):
    if testing:
        CoQA.run_test_bigTrain()
    else:
        CoQA.run_train()
        CoQA.run_dev()

def run_QuAC(testing):
    if testing:
        QuAC.run_test_trainBig()
    else:
        QuAC.run_train()
        QuAC.run_val()

def run_SQuAD(testing):
    if testing:
        SQuAD.run_test_trainBig()
    else:
        SQuAD.run_train()
        SQuAD.run_dev()

def run_WikiQA(testing):
    if testing:
        WikiQA.dev_test()
        WikiQA.pos_ans_test()
    else:
        WikiQA.dev()
        WikiQA.test()
        WikiQA.train()
        WikiQA.just()
        WikiQA.pos_ans()
