import deepl
import file_utils
import csv
import deepl 
import re
import preprocessing
import postprocessing
import json


class QuAC:
    def __init__(self, filename, isTesting):
        with open("auth.json") as f:
            data = json.load(f)
        
        self.translator = deepl.translator.Translator(
            auth_key=data.get("auth_key"), 
            source_lang=deepl.translator.Language.EN, 
            target_lang=deepl.translator.Language.DE)

        self.structure = {
                            "data": []
                        }

        if isTesting:
            self.input_file_path = "data/test_input/QuAC/" + filename
            self.output_file_path = "data/test_output/QuAC/" + filename
        else:
            self.input_file_path = "data/input_data/QuAC/" + filename
            self.output_file_path = "data/output_data/QuAC/" + filename
        
        self.intermediate_path_EN = "data/intermediate_data/EN_" + filename
        self.intermediate_path_DE = "data/intermediate_data/DE_" + filename
        self.intermediate_path_DE_full = "data/intermediate_data/DE_Full_" + filename
        self.final_result_path = "data/final_result/QuAC/" + filename

        self.data = file_utils.open_file(self.input_file_path)


    def runTranslation(self):

        try: 
            f = open(self.output_file_path)
            self.run_postprocessing()
        except FileNotFoundError as e:
            print("no OUT")
            try: 
                f = open(self.intermediate_path_DE_full)
                self.set_answer_span_text()
                self.set_answer_index()
                self.run_postprocessing()
            except FileNotFoundError as e:
                print("no DE_FULL")
                try:
                    f = open(self.intermediate_path_DE)
                    self.translate()
                    self.set_answer_span_text()
                    self.set_answer_index()
                    self.run_postprocessing()
                except FileNotFoundError as e:
                    print("no DE")
                    self.create_intermediate_data()
                    self.translate()
                    self.set_answer_span_text()
                    self.set_answer_index()
                    self.run_postprocessing()

            
    def count_signs(self):
        translated = self.data
        cnt = 0
        for data in translated.get("data"):
            for k,v in data.items():
                if k == "paragraphs":
                    paragraphs = data[k]
                    for paragraph in paragraphs:
                        for k2,v2 in paragraph.items():
                            if k2 == "context":
                                cnt += len(paragraph[k2])
                            if k2 == "qas":
                                qas = paragraph[k2]
                                for q in qas:
                                    for k3, v3 in q.items():
                                        if k3 == "question":
                                            cnt += len(q[k3])
                                        if k3 == "answers":
                                            answers = q[k3]
                                            for answer in answers:
                                                cnt += len(answer["text"])
                                        if k3 == "orig_answer":
                                            cnt += len(q[k3]["text"])

                if k == "section_title":
                    cnt += len(data[k])
                if k == "background":
                    cnt += len(data[k])

        return cnt

    

    def create_intermediate_data(self):
        spanIdList= []
        cnt_a = 0
        cnt_oa = 0
        
        intermediate = self.data
        for data in intermediate.get("data"):
            for k,v in data.items():
                if k == "paragraphs":
                    paragraphs = data[k]
                    for paragraph in paragraphs:
                        for k2, v2 in paragraph.items():
                            if k2 == "context":
                                paragraph[k2] = paragraph[k2] + " "
                                story = paragraph[k2]
                                origin_story = story
                            if k2 == "qas":
                                qas = paragraph[k2]
                                for q in qas:
                                    #temp_id = q["id"]
                                    for k3,v3 in q.items():
                                        if k3 == "answers":
                                            anno_type = "a"
                                            answers = q[k3]
                                            for answer in answers:
                                                temp_id = "q" + str(cnt_a)
                                                temp_span_text = answer["text"]
                                                temp_span_start = answer["answer_start"]
                                                temp_span_end = temp_span_start + len(temp_span_text) 

                                                           
                                                temp_span_start = preprocessing.handle_splitted_start_word(origin_story, temp_span_start)
                                                temp_span_end = preprocessing.handle_splitted_end_word(origin_story, temp_span_end)
                                                temp_span_start = preprocessing.update_temp_start(spanIdList, temp_span_start, len(preprocessing.get_start_annotation(temp_id, anno_type)))
                                                temp_span_end = preprocessing.update_temp_end(spanIdList, temp_span_end)
                                                story = preprocessing.insert_annotation(story, temp_id, temp_span_start, temp_span_end, anno_type)
                    
                                                temp_span_id_list = []
                                                temp_span_id_list.append(temp_span_start)
                                                temp_span_id_list.append(anno_type + str(temp_id))
                                                spanIdList = preprocessing.update_spanIdList(spanIdList, temp_span_id_list)
                                                spanIdList.append(temp_span_id_list)


                                                temp_span_id_list = []
                                                temp_span_id_list.append(temp_span_end +  len(preprocessing.get_start_annotation(temp_id, anno_type)))
                                                temp_span_id_list.append("\\" + anno_type + str(temp_id))
                                                spanIdList = preprocessing.update_spanIdList(spanIdList, temp_span_id_list)
                                                spanIdList.append(temp_span_id_list)
                                                

                                                paragraph["context"] = story 
                                                cnt_a += 1


                                        if k3 == "orig_answer":
                                            anno_type = "o"
                                            temp_id = "q" + str(cnt_oa)
                                            temp_span_text = q[k3]["text"]
                                            temp_span_start = q[k3]["answer_start"]
                                            temp_span_end = temp_span_start + len(temp_span_text) 

                                            
                                            temp_span_start = preprocessing.handle_splitted_start_word(origin_story, temp_span_start)
                                            temp_span_end = preprocessing.handle_splitted_end_word(origin_story, temp_span_end)
                                            temp_span_start = preprocessing.update_temp_start(spanIdList, temp_span_start, len(preprocessing.get_start_annotation(temp_id, anno_type)))
                                            temp_span_end = preprocessing.update_temp_end(spanIdList, temp_span_end)
                                            story = preprocessing.insert_annotation(story, temp_id, temp_span_start, temp_span_end, anno_type)
                
                                            temp_span_id_list = []
                                            temp_span_id_list.append(temp_span_start)
                                            temp_span_id_list.append(anno_type + str(temp_id))
                                            spanIdList = preprocessing.update_spanIdList(spanIdList, temp_span_id_list)
                                            spanIdList.append(temp_span_id_list)


                                            temp_span_id_list = []
                                            temp_span_id_list.append(temp_span_end +  len(preprocessing.get_start_annotation(temp_id, anno_type)))
                                            temp_span_id_list.append("\\" + anno_type + str(temp_id))
                                            spanIdList = preprocessing.update_spanIdList(spanIdList, temp_span_id_list)
                                            spanIdList.append(temp_span_id_list)
                                            
                                           
                                            paragraph["context"] = story 
                                            cnt_oa += 1


                    cnt_a = 0
                    cnt_oa = 0
                    spanIdList = []

        file_utils.write_file(self.intermediate_path_EN, intermediate )
        file_utils.write_file(self.intermediate_path_DE, self.structure)
    

                        
    def translate(self):
        try:
            data = file_utils.open_file(self.intermediate_path_EN)
            tmp_structure = file_utils.open_file(self.intermediate_path_DE)
        except Exception as e:
            print("Run create_intermediate_data before calling translate")

        while data.get("data"):
            d = data.get("data")[0]
            for k,v in d.items():
                if k == "paragraphs":
                    paragraphs = d[k]
                    for paragraph in paragraphs:
                        for k2,v2 in paragraph.items():
                            if k2 == "context":
                                paragraph[k2] = self.translator.translate_text(paragraph[k2])
                            if k2 == "qas":
                                qas = paragraph[k2]
                                for q in qas:
                                    for k3, v3 in q.items():
                                        if k3 == "question":
                                            q[k3] = self.translator.translate_text(q[k3])
                                       
                if k == "section_title":
                    d[k] = self.translator.translate_text(d[k])
                if k == "background":
                    d[k] = self.translator.translate_text(d[k])
            
            
            tmp_structure.get("data").append(d)
            file_utils.write_file(self.intermediate_path_DE, tmp_structure)
            data.get("data").pop(0)
            file_utils.write_file(self.intermediate_path_EN, data)

        file_utils.write_file(self.intermediate_path_DE_full, self.structure)




    
    def set_answer_span_text(self):

        try:
            translated_with_answers = file_utils.open_file(self.intermediate_path_DE)
            tmp_structure = file_utils.open_file(self.intermediate_path_DE_full)
        except Exception as e:
            print("Run translate before calling set_anwer_span_text")

        cnt_a = 0
        cnt_oa = 0
        
        while translated_with_answers.get("data"):
            data = translated_with_answers.get("data")[0]
            for k,v in data.items():
                if k == "paragraphs":
                    paragraphs = data[k]
                    for paragraph in paragraphs:
                        for k2, v2 in paragraph.items():
                            if k2 == "context":
                                story = paragraph[k2]
                            if k2 == "qas":
                                qas = paragraph[k2]
                                for q in qas:
                                    for k3,v3 in q.items():
                                        if k3 == "answers":
                                            anno_type = "a"
                                            answers = q[k3]
                                            for answer in answers:
                                                temp_id = "q" + str(cnt_a)
                                                
                                                anno_type = "a"
                                                tmp_start_annotation = preprocessing.get_start_annotation(temp_id, anno_type)
                                                tmp_end_annotation = preprocessing.get_end_annotation(temp_id, anno_type)
                                                start_index = preprocessing.find_index_of_annotation(story, tmp_start_annotation)
                                                end_index = preprocessing.find_index_of_annotation(story, tmp_end_annotation)
                                                orig_answer =  self.translator.translate_text(answer["text"])
                                                new_answer = preprocessing.handle_answer_span_text(start_index, end_index, orig_answer, story)
                                                answer["text"] = new_answer
                                                cnt_a += 1

                                        if k3 == "orig_answer":
                                            anno_type = "o"
                                            temp_id = "q" + str(cnt_oa)
                                            tmp_start_annotation = preprocessing.get_start_annotation(temp_id, anno_type)
                                            tmp_end_annotation = preprocessing.get_end_annotation(temp_id, anno_type)
                                            start_index = preprocessing.find_index_of_annotation(story, tmp_start_annotation)
                                            end_index = preprocessing.find_index_of_annotation(story, tmp_end_annotation)
                                            orig_answer =  self.translator.translate_text(q[k3]["text"])
                                            new_answer = preprocessing.handle_answer_span_text(start_index, end_index, orig_answer, story)
                                            q[k3]["text"] = new_answer
                                            cnt_oa += 1
                    cnt_a = 0
                    cnt_oa = 0

            tmp_structure.get("data").append(data)
            file_utils.write_file(self.intermediate_path_DE_full, tmp_structure)
            translated_with_answers.get("data").pop(0)
            file_utils.write_file(self.intermediate_path_DE, translated_with_answers)




    def set_answer_index(self):
        try:
            translated_with_indexed_answers = file_utils.open_file(self.intermediate_path_DE_full)
        except Exception as e:
            print("Run set_answer_span_text before calling set_answer_index")

        cnt_a = 0
        cnt_oa = 0
        
        for data in translated_with_indexed_answers.get("data"):
            for k,v in data.items():
                if k == "paragraphs":
                    paragraphs = data[k]
                    for paragraph in paragraphs:
                        for k2, v2 in paragraph.items():
                            if k2 == "context":
                                story = paragraph[k2]
                                story = preprocessing.remove_all_xml_tag(story)
                                paragraph[k2] = story
                            if k2 == "qas":
                                qas = paragraph[k2]
                                for q in qas:
                                    for k3,v3 in q.items():
                                        if k3 == "answers":
                                            anno_type = "a"
                                            answers = q[k3]
                                            for answer in answers:
                                                span_text = answer["text"]
                                                try:
                                                    new_span_start = story.index(span_text)
                                                    
                                                except Exception as e:
                                                    new_span_start = -1

                                                answer["answer_start"] = new_span_start

                                        if k3 == "orig_answer":
                                            span_text = q[k3]["text"]
                                            try:
                                                new_span_start = story.index(span_text)
                                            except Exception as e:
                                                new_span_start = -1
                                                
                                            q[k3]["answer_start"] = new_span_start

        file_utils.write_file(self.output_file_path, translated_with_indexed_answers)
        file_utils.remove_allFiles("data/intermediate_data")   


    def run_postprocessing(self):
        try:
            quac_data = file_utils.open_file(self.output_file_path)
        except Exception as e:
                print("Run set_answer_span_text before calling run_postprocessing")

        for data in quac_data.get("data"):
                for k,v in data.items():
                    if k == "paragraphs":
                        paragraphs = data[k]
                        for paragraph in paragraphs:
                            for k2,v2 in paragraph.items():
                                if k2 == "context":
                                    story = paragraph[k2]
                                if k2 == "qas":
                                    qas = paragraph[k2]
                                    for q in qas:
                                        for k3, v3 in q.items():
                                            if k3 == "answers":
                                                answers = q[k3]
                                                for answer in answers:
                                                    if answer["answer_start"]==-1 and answer["text"] != "CANNOTANSWER":
                                                        new_span_text = postprocessing.longest_common_substring(answer["text"], story)
                                                        if len(new_span_text) > 0:
                                                            new_span_start = story.find(new_span_text)
                                                            answer["text"] = new_span_text
                                                            answer["answer_start"] = new_span_start
                                            if k3 == "orig_answer":
                                                if q[k3]["answer_start"] == -1 and q[k3]["text"] != "CANNOTANSWER":
                                                    new_span_text = postprocessing.longest_common_substring(q[k3]["text"], story)
                                                    if len(new_span_text) > 0:
                                                        new_span_start = story.find(new_span_text)
                                                        q[k3]["text"] = new_span_text
                                                        q[k3]["answer_start"] = new_span_start
        
        file_utils.write_file(self.final_result_path, quac_data)
        file_utils.remove_file(self.output_file_path)



def run_test_val():
    filename = "val.json"
    isTesting = True 
    q = QuAC(filename, isTesting)
    q.runTranslation()

def run_test_train():
    filename = "train.json"
    isTesting = True 
    q = QuAC(filename, isTesting)
    q.runTranslation()

def run_test_trainBig():
    filename = "trainBig.json"
    isTesting = True 
    q = QuAC(filename, isTesting)
    q.runTranslation()


def run_train():
    filename = "train.json"
    isTesting = False
    q = QuAC(filename, isTesting)
    q.runTranslation()

def run_val():
    filename = "val.json"
    isTesting = False
    q = QuAC(filename, isTesting)
    q.runTranslation()