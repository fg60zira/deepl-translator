import json
from deepdiff import DeepDiff
import os, shutil
import pandas as pd
from shutil import copyfile


def copy_file(input_filename, output_filename):
    copyfile(input_filename, output_filename)



def open_file(input_file_path):
        with open(file=input_file_path) as f:
            data = json.load(f)
            return data


def write_file(output_file_path, data):
    json_object = json.dumps(data, indent = 2, ensure_ascii= False) 
    with open(output_file_path, "w") as f:
        f.write(json_object)


def indent_json(input_file_path, output_file_path):
    with open(file=input_file_path) as f:
        data = json.load(f)
    with open(output_file_path, "w") as w:
        data = json.dumps(data, indent=2, ensure_ascii= False)
        w.write(data)


def diff_json_files(input1, input2):
    ddiff = DeepDiff(input1, input2, ignore_order=False)
    return ddiff

def remove_allFiles(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try: 
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

def remove_file(file_path):
    os.remove(file_path)
