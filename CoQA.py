import deepl
import file_utils
import csv
import deepl 
import re
import deepl
import file_utils
import csv
import deepl 
import re
import preprocessing
import json
import os
import shutil
import postprocessing


class CoQA:
    
    def __init__(self, filename, isTesting):
        with open("auth.json") as f:
            data = json.load(f)

        self.translator = deepl.translator.Translator(
            auth_key=data.get("auth_key"), 
            source_lang=deepl.translator.Language.EN, 
            target_lang=deepl.translator.Language.DE)

        self.structure = {
                        "version": "1.0",
                            "data": []
                        }

        if isTesting:
            self.input_file_path = "data/test_input/CoQA/" + filename
            self.output_file_path = "data/test_output/CoQA/" + filename
        else:
            self.input_file_path = "data/input_data/CoQA/" + filename
            self.output_file_path = "data/output_data/CoQA/" + filename
        
        self.intermediate_path_EN = "data/intermediate_data/EN_" + filename
        self.intermediate_path_DE = "data/intermediate_data/DE_" + filename
        self.intermediate_path_DE_full = "data/intermediate_data/DE_Full_" + filename
        self.final_result_path = "data/final_result/CoQA/" + filename

        self.data = file_utils.open_file(self.input_file_path)
        

    def runTranslation(self):
        try: 
            f = open(self.output_file_path)
            self.run_postprocessing()
        except FileNotFoundError as e:
                print("no OUT")
                try: 
                    f = open(self.intermediate_path_DE_full)
                    self.set_answer_span_text()
                    self.set_answer_index()
                    self.run_postprocessing()
                except FileNotFoundError as e:
                    print("no DE_FULL")
                    try:
                        f = open(self.intermediate_path_DE)
                        self.translate()
                        self.set_answer_span_text()
                        self.set_answer_index()
                        self.run_postprocessing()
                    except FileNotFoundError as e:
                        print("no DE")
                        self.create_intermediate_data()
                        self.translate()
                        self.set_answer_span_text()
                        self.set_answer_index()
                        self.run_postprocessing()
        



    def count_signs(self):
        translated = self.data
        cnt = 0
        
        for data in translated.get("data"):
            for k,v in data.items():
                if k == "story":
                    cnt += len(data[k]) 
                if k == "questions":
                    questions = data[k]
                    for question in questions:
                        cnt += len(question["input_text"])
                if k == "answers":
                    answers = data[k]
                    for answer in answers:
                        cnt += len(answer["span_text"])
                        cnt += len(answer["input_text"])
                if k == "additional_answers":
                    additional_answers = data[k]
                    for k2, v2 in additional_answers.items():
                        values = additional_answers[k2]
                        for value in values:
                            cnt += len(value["span_text"])
                            cnt += len(value["input_text"])
    
        return cnt  




    def create_intermediate_data(self):
        temp_span_start = 0
        temp_span_end = 0
        spanIdList= []

        intermediate = self.data
        for data in intermediate.get("data"):
            for k,v in data.items():
                if k == "story":
                    data[k] = data[k] + " "
                    story = data[k]
                    origin_story = story
                if k == "answers":
                    answers = data[k]
                    anno_type = "a"
                    for answer in answers:
                        temp_span_text = answer["span_text"]
                        temp_span_start = answer["span_start"]
                        temp_id = answer["turn_id"]
                        temp_span_end = temp_span_start + len(temp_span_text)
                                               
                        if temp_span_start >= 0:
                            temp_span_start = preprocessing.handle_splitted_start_word(origin_story, temp_span_start)
                            temp_span_end = preprocessing.handle_splitted_end_word(origin_story, temp_span_end)
                            temp_span_start = preprocessing.update_temp_start(spanIdList, temp_span_start, len(preprocessing.get_start_annotation(temp_id, anno_type)))
                            temp_span_end = preprocessing.update_temp_end(spanIdList, temp_span_end)
                            story = preprocessing.insert_annotation(story, temp_id, temp_span_start, temp_span_end, anno_type)

                            temp_span_id_list = []
                            temp_span_id_list.append(temp_span_start)
                            temp_span_id_list.append(anno_type + str(temp_id))
                            spanIdList = preprocessing.update_spanIdList(spanIdList, temp_span_id_list)
                            spanIdList.append(temp_span_id_list)


                            temp_span_id_list = []
                            temp_span_id_list.append(temp_span_end +  len(preprocessing.get_start_annotation(temp_id, anno_type)))
                            temp_span_id_list.append("\\" + anno_type + str(temp_id))
                            spanIdList = preprocessing.update_spanIdList(spanIdList, temp_span_id_list)
                            spanIdList.append(temp_span_id_list)
                            data["story"] = story

                if k == "additional_answers":
                    additional_answers = data[k]
                    for k2, v2 in additional_answers.items():
                        values = additional_answers[k2]
                        anno_type = "o" + str(k2)
                        for value in values:
                            temp_span_text = value["span_text"]
                            temp_span_start = value["span_start"]
                            temp_id = value["turn_id"]
                            temp_span_end = temp_span_start + len(temp_span_text) 
                           
                            if temp_span_start >= 0:
                                temp_span_start = preprocessing.handle_splitted_start_word(origin_story, temp_span_start)
                                temp_span_end = preprocessing.handle_splitted_end_word(origin_story, temp_span_end)
                                temp_span_start = preprocessing.update_temp_start(spanIdList, temp_span_start, len(preprocessing.get_start_annotation(temp_id, anno_type)))
                                temp_span_end = preprocessing.update_temp_end(spanIdList, temp_span_end)
                                story = preprocessing.insert_annotation(story, temp_id, temp_span_start, temp_span_end, anno_type)
    
                                temp_span_id_list = []
                                temp_span_id_list.append(temp_span_start)
                                temp_span_id_list.append(anno_type + str(temp_id))
                                spanIdList = preprocessing.update_spanIdList(spanIdList, temp_span_id_list)
                                spanIdList.append(temp_span_id_list)


                                temp_span_id_list = []
                                temp_span_id_list.append(temp_span_end +  len(preprocessing.get_start_annotation(temp_id, anno_type)))
                                temp_span_id_list.append("\\" + anno_type + str(temp_id))
                                spanIdList = preprocessing.update_spanIdList(spanIdList, temp_span_id_list)
                                spanIdList.append(temp_span_id_list)
                                                
                                data["story"] = story
                    
                        
            spanIdList= []

        file_utils.write_file(self.intermediate_path_EN, intermediate )
        file_utils.write_file(self.intermediate_path_DE, self.structure)
    

    def translate(self):
        try:
            data = file_utils.open_file(self.intermediate_path_EN)
            tmp_structure = file_utils.open_file(self.intermediate_path_DE)
        except Exception as e:
            print("Run create_intermediate_data before calling translate")

        while data.get("data"):
            d = data.get("data")[0]
            for k,v in d.items():
                if k == "story":
                    d[k] = self.translator.translate_text(d[k])
                if k == "questions":
                    questions = d[k]
                    for question in questions:
                        question["input_text"] = self.translator.translate_text(question["input_text"])
                if k == "answers":
                    answers = d[k]
                    for answer in answers:
                        answer["input_text"] = self.translator.translate_text(answer["input_text"])
                if k == "additional_answers":
                    additional_answers = d[k]
                    for k2, v2 in additional_answers.items():
                        values = additional_answers[k2]
                        for value in values:
                            value["input_text"] = self.translator.translate_text(value["input_text"])
            
            tmp_structure.get("data").append(d)
            file_utils.write_file(self.intermediate_path_DE, tmp_structure)
            data.get("data").pop(0)
            file_utils.write_file(self.intermediate_path_EN, data)

        file_utils.write_file(self.intermediate_path_DE_full, self.structure)




    
    def set_answer_span_text(self):
        try:
            translated_with_answers = file_utils.open_file(self.intermediate_path_DE)
            tmp_structure = file_utils.open_file(self.intermediate_path_DE_full)
        except Exception as e:
            print("Run translate before calling set_anwer_span_text")
        
        while translated_with_answers.get("data"):
            data = translated_with_answers.get("data")[0]
            for k,v in data.items():
                if k == "story":
                    story = data[k]
                
                if k == "answers":
                    answers = data[k]
                    for answer in answers:
                        temp_span_start = answer["span_start"]
                        temp_id = answer["turn_id"]
                        
                        if temp_span_start >= 0:
                            anno_type = "a"
                            tmp_start_annotation = preprocessing.get_start_annotation(temp_id, anno_type)
                            tmp_end_annotation = preprocessing.get_end_annotation(temp_id, anno_type)
                            start_index = preprocessing.find_index_of_annotation(story, tmp_start_annotation)
                            end_index = preprocessing.find_index_of_annotation(story, tmp_end_annotation)
                            orig_answer =  self.translator.translate_text(answer["span_text"])
                            new_answer = preprocessing.handle_answer_span_text(start_index, end_index, orig_answer, story)
                            answer["span_text"] = new_answer

                        else:
                            answer["span_text"] = "unbekannt"

                if k == "additional_answers":
                    additional_answers = data[k]
                    for k2, v2 in additional_answers.items():
                        anno_type = "o" + str(k2)
                        values = additional_answers[k2]
                        for value in values:
                            temp_span_start = value["span_start"]
                            temp_id = value["turn_id"]
                            
                            if temp_span_start >= 0:
                                tmp_start_annotation = preprocessing.get_start_annotation(temp_id, anno_type)
                                tmp_end_annotation = preprocessing.get_end_annotation(temp_id, anno_type)
                                start_index = preprocessing.find_index_of_annotation(story, tmp_start_annotation)
                                end_index = preprocessing.find_index_of_annotation(story, tmp_end_annotation)
                                orig_answer =  self.translator.translate_text(value["span_text"])
                                new_answer = preprocessing.handle_answer_span_text(start_index, end_index, orig_answer, story)
                                value["span_text"] = new_answer

                            else:
                                value["span_text"]= "unbekannt"

            tmp_structure.get("data").append(data)
            file_utils.write_file(self.intermediate_path_DE_full, tmp_structure)
            translated_with_answers.get("data").pop(0)
            file_utils.write_file(self.intermediate_path_DE, translated_with_answers)


    def set_answer_index(self):
        try:
            translated_with_indexed_answers = file_utils.open_file(self.intermediate_path_DE_full)
        except Exception as e:
            print("Run set_answer_span_text before calling set_answer_index")

        for data in translated_with_indexed_answers.get("data"):
            for k,v in data.items():
                if k == "story":
                    story = data[k]
                    story = preprocessing.remove_all_xml_tag(story)
                    data[k] = story
                if k == "answers":
                    answers = data[k]
                    for answer in answers:
                        span_text = answer["span_text"]

                        try:
                            new_span_start = story.index(span_text)
                            new_span_end = new_span_start + len(span_text)
                            
                        except Exception as e:
                            new_span_start = -1
                            new_span_end = -1
                        
                        answer["span_start"] = new_span_start
                        answer["span_end"] = new_span_end

                        


                if k == "additional_answers":
                    additional_answers = data[k]
                    for k2, v2 in additional_answers.items():
                        values = additional_answers[k2]
                        for value in values:
                            span_text = value["span_text"]
                        
                        try:
                            new_span_start = story.index(span_text)
                            new_span_end = new_span_start + len(span_text)
                            
                        except Exception as e:
                            new_span_start = -1
                            new_span_end = -1
                        
                        answer["span_start"] = new_span_start
                        answer["span_end"] = new_span_end

     
        file_utils.write_file(self.output_file_path, translated_with_indexed_answers)
        file_utils.remove_allFiles("data/intermediate_data")          


    def run_postprocessing(self):
        try:
            coqa_data = file_utils.open_file(self.output_file_path)
        except Exception as e:
            print("Run set_answer_span_text before calling run_postprocessing")

        for data in coqa_data.get("data"):
            for k,v in data.items():
                if k == "story":
                    story = data[k]
                if k == "answers":
                    answers = data[k]
                    for answer in answers:
                        if answer["span_start"]==-1 and answer["span_text"] != "unbekannt":
                            new_span_text = postprocessing.longest_common_substring(answer["span_text"], story)
                            if len(new_span_text) > 0:
                                new_span_start = story.find(new_span_text)
                                new_span_end = new_span_start + len(new_span_text)
                                answer["span_text"] = new_span_text
                                answer["span_start"] = new_span_start
                                answer["span_end"] = new_span_end
            
                if k == "additional_answers":
                    add_answers = data[k]
                    for k2, v2 in add_answers.items():
                        answers = add_answers[k2]
                        for answer in answers:
                            if answer["span_start"]==-1 and answer["span_text"] != "unbekannt":
                                new_span_text = postprocessing.longest_common_substring(answer["span_text"], story)
                                if len(new_span_text) > 0:
                                    new_span_start = story.find(new_span_text)
                                    new_span_end = new_span_start + len(new_span_text)
                                    answer["span_text"] = new_span_text
                                    answer["span_start"] = new_span_start
                                    answer["span_end"] = new_span_end
        
        file_utils.write_file(self.final_result_path, coqa_data)
        file_utils.remove_file(self.output_file_path)
    

def run_test_dev():
    filename = "dev.json"
    isTesting = True
    c = CoQA(filename, isTesting ) 
    c.runTranslation()

def run_test_train():
    filename = "train.json"
    isTesting = True
    c = CoQA(filename, isTesting) 
    c.runTranslation()

def run_test_bigTrain():
    filename = "bigTrain.json"
    isTesting = True
    c = CoQA(filename, isTesting) 
    c.runTranslation()

def run_test_medium():
    filename = "medium.json"
    isTesting = True
    c = CoQA(filename, isTesting) 
    c.runTranslation()


def run_train():
    filename = "train.json"
    isTesting = False
    c = CoQA(filename, isTesting) 
    c.runTranslation()

def run_dev():
    filename = "dev.json"
    isTesting = False
    c = CoQA(filename, isTesting)
    c.runTranslation()



