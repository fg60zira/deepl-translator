# deepl-translator

### Datasets
* WikiQA: https://www.microsoft.com/en-us/download/details.aspx?id=52419
* coqa: https://stanfordnlp.github.io/coqa/
* quac: https://quac.ai/
* SQuAD (1.1 & 2.0): https://rajpurkar.github.io/SQuAD-explorer/

## usage
* create folder input_data with subfolders CoQA, QuAC, SQuAD, WikiQACorpus containing the diffrent datasets
* create folder output_data analog with the empty subfolders
* create folder intermediate_data analog with empty subfolders
  
  