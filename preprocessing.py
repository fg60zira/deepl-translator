import re

allowed_signs =  [" ", ",",".","!", ":","?",";", "(", ")", "-" , "\\" , "\"", "'", '"', "\n", "$"]

def remove_all_xml_tag(text):
    text = re.sub('<a[^>]*>', '', text)
    text = re.sub('<o[^>]*>', '', text)
    text = re.sub('</a[^>]*>', '', text)
    text = re.sub('</o[^>]*>', '', text)
    return text

def handle_splitted_start_word(story, span_start):
    if span_start == 0:
        return span_start
    temp_sign = story[span_start]
    temp_sign_added = story[span_start-1]
    if temp_sign in allowed_signs: 
        return span_start + 1
    elif temp_sign_added in allowed_signs: 
        return span_start
    else:
        startOfWord = story.rfind(" ", 0, span_start) + 1
        return startOfWord

def handle_splitted_end_word(story, span_end):
    story_end_index = len(story) 
    temp_sign = story[span_end]
    temp_sign_reduced = story[span_end - 1]
    if temp_sign in allowed_signs: 
        return span_end
    elif temp_sign_reduced in allowed_signs: 
        return span_end - 1
    else:
        endOfWord = story.find(" ", span_end, story_end_index) 
        return endOfWord


def get_start_annotation(id, anno_type):
    anno = "<" + anno_type + str(id) + ">"
    return anno 


def get_end_annotation(id, anno_type):
    anno = "</" + anno_type + str(id) + ">"
    return anno 


def insert_annotation(story, id, span_start, span_end, anno_type):
    start_anno = get_start_annotation(id, anno_type)
    end_anno = get_end_annotation(id, anno_type)
    story = story[:span_start] + start_anno + story[span_start:span_end] + end_anno + story[span_end:]
    return story


def find_index_of_annotation(story, annotation):
    idx = story.find(annotation, 0)
    return idx


def update_temp_start(spanIdList, temp_span_start, anno_length):
    spanIdList = sorted(spanIdList, key = lambda x:x[0])
    for span in spanIdList:
        if span[0] <= temp_span_start:
            temp_span_start += len(get_start_annotation(span[1], ""))
    return temp_span_start


def update_temp_end(spanIdList, temp_span_end):
    spanIdList = sorted(spanIdList, key = lambda x:x[0])
    for span in spanIdList:
        if span[0] <= temp_span_end:
            temp_span_end += len(get_start_annotation(span[1], ""))
    return temp_span_end


def update_spanIdList(spanIDList, temp_span_id_list):
    for span in spanIDList:
        if span[0] >= temp_span_id_list[0]:
            span[0] = span[0] + len(get_start_annotation(temp_span_id_list[1], ""))
    return spanIDList


def correct_span_end(origin_story, span_end):
    story_length = len(origin_story)
    story_end_index = story_length - 1 
    if span_end <= story_end_index:
        return span_end
    else:
        return story_end_index

def handle_answer_span_text(start_index, end_index, orig_answer, story):
    if start_index < 0 or end_index < 0:
        return orig_answer
    else:
        newStory = remove_all_xml_tag(story)
        try:
            i = newStory.index(orig_answer)
            return orig_answer
        except Exception as e:
            tmp_answer = story[start_index:end_index]
            cleaned_answer = remove_all_xml_tag(tmp_answer)
            cleaned_answer = cleaned_answer.strip()

            # sometimes the tags shifts during translation -> just surrounding a dot 
            if len(orig_answer) > 1 and len(cleaned_answer) <= 1:
                return orig_answer               
            return cleaned_answer

